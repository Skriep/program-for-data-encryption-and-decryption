#include "Byte.h"

Byte::Byte(const unsigned char& byte) { m_byte = byte; }

bool Byte::operator[](std::size_t index) { return m_byte & (0b1 << index); }

bool Byte::operator[](std::size_t index) const {
	return m_byte & (0b1 << index);
}

bool Byte::operator<(const Byte& b) const {
	return (Byte::compare(*this, b) == -1);
}

bool Byte::operator>(const Byte& b) const { return (b < *this); }

bool Byte::operator<=(const Byte& b) const { return !(b < *this); }

bool Byte::operator>=(const Byte& b) const { return !(*this < b); }

bool Byte::operator==(const Byte& b) const {
	return (Byte::compare(*this, b) == 0);
}

bool Byte::operator!=(const Byte& b) const { return !(*this == b); }

bool Byte::operator==(const unsigned char& byte) const {
	return (m_byte == byte);
}

bool Byte::operator!=(const unsigned char& byte) const {
	return (m_byte != byte);
}

Byte Byte::operator~() const { return ~m_byte; }

Byte& Byte::operator<<=(std::size_t n) {
	m_byte <<= n;
	return *this;
}

Byte& Byte::operator>>=(std::size_t n) {
	m_byte >>= n;
	return *this;
}

Byte& Byte::operator^=(const unsigned char& byte) {
	m_byte ^= byte;
	return *this;
}

Byte& Byte::operator|=(const unsigned char& byte) {
	m_byte |= byte;
	return *this;
}

void Byte::serialize(std::ostream& stream) {
	stream.write((char*)&m_byte, sizeof(m_byte));
}

Byte Byte::deserialize(std::istream& stream) {
	unsigned char byte = 0;
	stream.read((char*)&byte, sizeof(byte));
	return byte;
}

Byte Byte::sum(const Byte& a, const Byte& b, bool& carry, bool& prev_carry) {
	unsigned char byte = 0;
	for (std::size_t j = 0; j < 8; j++) {
		prev_carry = carry;
		bool x = a[j] ^ b[j];
		bool bit = x ^ carry;
		byte |= (bit << j);
		carry = (a[j] && b[j]) || (x && carry);
	}
	return byte;
}

int Byte::compare(const Byte& a, const Byte& b) {
	std::size_t j = 8;
	while (j-- > 0) {
		if (a[j] != b[j]) {
			return (a[j] ? 1 : -1);
		}
	}
	return 0;
}
