#if !defined(BigIntegerH)
#define BigIntegerH

#include <cstddef>
#include <iostream>
#include <memory>
#include <sstream>
#include <stdexcept>

#include "Byte.h"

class BigInteger {
   private:
	// Bytes in order from LSbyte (index 0) to MSbyte
	std::unique_ptr<Byte[]> m_bytes = std::make_unique<Byte[]>(1);

	// Number of bytes
	std::size_t m_size = 1;

   public:
	BigInteger() = default;
	template <typename T>
	BigInteger(T n);
	BigInteger(const BigInteger& n);

	// Returns number of bytes
	std::size_t getSize() const;

	// Changes number of bytes to new_size
	bool resize(std::size_t new_size);

	// Reduces number of bytes to minimum
	bool reduce();

	void printBytes() const;

	bool operator<(const BigInteger& b) const;
	bool operator>(const BigInteger& b) const;
	bool operator<=(const BigInteger& b) const;
	bool operator>=(const BigInteger& b) const;
	bool operator==(const BigInteger& b) const;
	bool operator!=(const BigInteger& b) const;
	BigInteger& operator++();
	BigInteger operator+(const BigInteger& b) const;
	BigInteger& operator+=(const BigInteger& b);
	BigInteger operator-() const;
	BigInteger operator-(const BigInteger& b) const;
	BigInteger& operator-=(const BigInteger& b);
	BigInteger operator*(const BigInteger& b) const;
	BigInteger& operator*=(const BigInteger& b);
	BigInteger operator/(const BigInteger& b) const;
	BigInteger& operator/=(const BigInteger& b);
	BigInteger operator%(const BigInteger& b) const;
	BigInteger& operator%=(const BigInteger& b);
	BigInteger& operator<<=(std::size_t n);
	BigInteger& operator>>=(std::size_t n);
	BigInteger operator=(const BigInteger& n);

	void serialize(std::ostream& stream);
	static BigInteger deserialize(std::istream& stream);

	static int compare(const BigInteger& a, const BigInteger& b);
	static BigInteger div(const BigInteger& a, const BigInteger& b,
	                      BigInteger& rem);

	friend class CryptoMath;
	friend class RSA;
};

template <typename T>
BigInteger::BigInteger(T n) {
	if (!resize(sizeof(n))) {
		throw std::runtime_error("Cannot resize BigInteger");
	}
	for (std::size_t i = 0; i < sizeof(n); i++) {
		m_bytes[i] = n & 0b11111111;
		n >>= 8;
	}
}

#endif  // BigIntegerH