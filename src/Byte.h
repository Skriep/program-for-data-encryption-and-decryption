#if !defined(ByteH)
#define ByteH

#include <cstddef>
#include <sstream>

class Byte {
   private:
	unsigned char m_byte = 0;

   public:
	Byte() = default;
	Byte(const unsigned char& byte);

	// Returns corresponding bit
	// Note: bits are indexed from LSbit (index 0) to MSbit (index 7)
	bool operator[](std::size_t index);
	bool operator[](std::size_t index) const;

	bool operator<(const Byte& b) const;
	bool operator>(const Byte& b) const;
	bool operator<=(const Byte& b) const;
	bool operator>=(const Byte& b) const;
	bool operator==(const Byte& b) const;
	bool operator!=(const Byte& b) const;
	bool operator==(const unsigned char& byte) const;
	bool operator!=(const unsigned char& byte) const;

	Byte operator~() const;
	Byte& operator<<=(std::size_t n);
	Byte& operator>>=(std::size_t n);
	Byte& operator^=(const unsigned char& byte);
	Byte& operator|=(const unsigned char& byte);

	void serialize(std::ostream& stream);
	static Byte deserialize(std::istream& stream);

	static Byte sum(const Byte& a, const Byte& b, bool& carry,
	                bool& prev_carry);

	// Compare as unsigned bytes
	static int compare(const Byte& a, const Byte& b);
};

#endif  // ByteH