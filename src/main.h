#include <argparse/argparse.hpp>
#include <fstream>
#include <iostream>
#include <sstream>

#include "RSA.h"

void printUsage(const std::string& prog_name);
void printCommands(const std::string& prog_name);