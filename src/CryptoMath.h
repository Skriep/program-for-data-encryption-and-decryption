#if !defined(CryptoMathH)
#define CryptoMathH

#include <random>

#include "BigInteger.h"

class CryptoMath {
   public:
	// Returns base raised to power exp modulo mod
	static BigInteger binpow(const BigInteger& base, const BigInteger& exp,
	                         const BigInteger& mod);

	static BigInteger inverse(BigInteger x, const BigInteger& m);

	// Miller–Rabin primality test
	static bool isPrime(const BigInteger& number, int rounds, bool print_state);

	static BigInteger getRandomPrime(std::size_t bits_count, bool print_state);
	template <typename T>
	static BigInteger getRandomNumber(T& rand_engine, std::size_t bits_count);
};

template <typename T>
BigInteger CryptoMath::getRandomNumber(T& rand_engine, std::size_t bits_count) {
	if (bits_count < 2) {
		throw std::runtime_error(
			"Cannot generate random number of size < 2 bits");
	}
	BigInteger res;
	if (!res.resize(bits_count / 8 + 1)) {
		throw std::runtime_error("Cannot resize BigInteger");
	}

	std::uniform_int_distribution<unsigned char> dist;
	for (std::size_t byte_num = 0; byte_num < res.m_size - 1; byte_num++) {
		res.m_bytes[byte_num] = dist(rand_engine);
	}
	unsigned char mask = 0;
	for (int i = 0; i < bits_count % 8; i++) {
		mask <<= 1;
		mask |= 1;
	}
	if (mask == 0) {
		res.m_bytes[res.m_size - 2] |= (1 << 7);
	} else {
		res.m_bytes[res.m_size - 1] = dist(rand_engine) & mask;
		res.m_bytes[res.m_size - 1] |= (1 << (bits_count % 8 - 1));
	}
	return res;
}

#endif  // CryptoMathH
