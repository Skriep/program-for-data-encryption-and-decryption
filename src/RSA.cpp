#include "RSA.h"

RSA::RSA(const BigInteger& modulus) { m_modulus = modulus; }

RSA::RSA(const BigInteger& modulus, const BigInteger& public_exp,
         const BigInteger& private_exp) {
	m_modulus = modulus;
	m_public_exp = std::make_unique<BigInteger>(public_exp);
	m_private_exp = std::make_unique<BigInteger>(private_exp);
}

RSA::RSA(const RSA& rsa) {
	m_modulus = rsa.m_modulus;
	if (rsa.m_public_exp) {
		m_public_exp = std::make_unique<BigInteger>(*rsa.m_public_exp);
	}
	if (rsa.m_private_exp) {
		m_private_exp = std::make_unique<BigInteger>(*rsa.m_private_exp);
	}
}

void RSA::setPublicExponent(const BigInteger& public_exp) {
	m_public_exp = std::make_unique<BigInteger>(public_exp);
}

void RSA::setPrivateExponent(const BigInteger& private_exp) {
	m_private_exp = std::make_unique<BigInteger>(private_exp);
}

BigInteger RSA::encrypt(const BigInteger& msg) {
	if (!m_public_exp) {
		throw std::runtime_error(
			"Cannot encrypt: public exponent is not specified");
	}
	return CryptoMath::binpow(msg, *m_public_exp, m_modulus);
}

void RSA::encrypt(std::istream& data_in, std::ostream& encrypted_out) {
	std::streampos pos = data_in.tellg();
	data_in.seekg(0, std::ios::end);
	std::size_t data_len = data_in.tellg();
	data_in.seekg(pos, std::ios::beg);
	m_modulus.reduce();
	if (m_modulus.getSize() < 3) {
		throw std::runtime_error("Cannot encrypt: modulus is too small");
	}
	std::size_t chunk_len = m_modulus.getSize() - 2;

	while (true) {
		if (chunk_len + data_in.tellg() > data_len) {
			if (data_in.tellg() >= data_len) {
				break;
			}
			chunk_len = data_len - data_in.tellg();
		}
		BigInteger chunk;
		chunk.resize(chunk_len + 1);
		chunk.m_bytes[chunk_len] = 0b1;
		for (std::size_t i = 0; i < chunk_len; i++) {
			chunk.m_bytes[i] = Byte::deserialize(data_in);
		}
		BigInteger encrypted = encrypt(chunk);
		encrypted.reduce();
		encrypted.serialize(encrypted_out);
	}
}

BigInteger RSA::decrypt(const BigInteger& enc) {
	if (!m_private_exp) {
		throw std::runtime_error(
			"Cannot decrypt: private exponent is not specified");
	}
	return CryptoMath::binpow(enc, *m_private_exp, m_modulus);
}

void RSA::decrypt(std::istream& encrypted_in, std::ostream& data_out) {
	std::streampos pos = encrypted_in.tellg();
	encrypted_in.seekg(0, std::ios::end);
	std::size_t encrypted_len = encrypted_in.tellg();
	encrypted_in.seekg(pos, std::ios::beg);
	while (encrypted_in.tellg() < encrypted_len) {
		BigInteger encrypted = BigInteger::deserialize(encrypted_in);
		BigInteger chunk = decrypt(encrypted);
		chunk.reduce();
		if (chunk.m_bytes[chunk.m_size - 1] != 0b1) {
			throw std::runtime_error(
				"Cannot decrypt: data is corrupted or invalid key is used");
		}
		for (std::size_t i = 0; i < chunk.m_size - 1; i++) {
			chunk.m_bytes[i].serialize(data_out);
		}
	}
}

RSA RSA::generateKeyPair(std::size_t mod_bits_count) {
	if (mod_bits_count < 24) {
		throw std::runtime_error(
			"Number of modulus bits is too small (less than 24)");
	}
	std::cout << "Generating prime p..." << std::endl;
	BigInteger p = CryptoMath::getRandomPrime(mod_bits_count / 2, true);
	std::cout << "Generating prime q..." << std::endl;
	BigInteger q =
		CryptoMath::getRandomPrime(mod_bits_count - mod_bits_count / 2, true);
	BigInteger e = 0x10001;
	BigInteger n = p * q;
	n.reduce();
	BigInteger phi = (p - 1) * (q - 1);
	BigInteger d = CryptoMath::inverse(e, phi);
	return RSA(n, e, d);
}

RSA RSA::operator=(const RSA& rsa) {
	m_modulus = rsa.m_modulus;
	if (rsa.m_public_exp) {
		m_public_exp = std::make_unique<BigInteger>(*rsa.m_public_exp);
	} else {
		m_public_exp = nullptr;
	}
	if (rsa.m_private_exp) {
		m_private_exp = std::make_unique<BigInteger>(*rsa.m_private_exp);
	} else {
		m_private_exp = nullptr;
	}
	return *this;
}

void RSA::serializePublicKey(std::ostream& stream) {
	m_modulus.serialize(stream);
	if (!m_public_exp) {
		throw std::runtime_error(
			"Cannot serialize public key: public exponent is not specified");
	}
	m_public_exp->serialize(stream);
}

void RSA::serializePrivateKey(std::ostream& stream) {
	m_modulus.serialize(stream);
	if (!m_private_exp) {
		throw std::runtime_error(
			"Cannot serialize private key: private exponent is not specified");
	}
	m_private_exp->serialize(stream);
}

void RSA::serializeKeyPair(std::ostream& stream) {
	RSA::serializePublicKey(stream);
	if (!m_private_exp) {
		throw std::runtime_error(
			"Cannot serialize private key: private exponent is not specified");
	}
	m_private_exp->serialize(stream);
}

RSA RSA::deserializePublicKey(std::istream& stream) {
	BigInteger modulus = BigInteger::deserialize(stream);
	RSA result = RSA(modulus);
	BigInteger public_exp = BigInteger::deserialize(stream);
	result.setPublicExponent(public_exp);
	return result;
}

RSA RSA::deserializePrivateKey(std::istream& stream) {
	BigInteger modulus = BigInteger::deserialize(stream);
	RSA result = RSA(modulus);
	BigInteger private_exp = BigInteger::deserialize(stream);
	result.setPrivateExponent(private_exp);
	return result;
}

RSA RSA::deserializePrivateOrPair(std::istream& stream) {
	std::streampos pos = stream.tellg();
	stream.seekg(0, std::ios::end);
	std::size_t stream_size = stream.tellg();
	stream.seekg(pos, std::ios::beg);
	BigInteger modulus = BigInteger::deserialize(stream);
	RSA result = RSA(modulus);
	BigInteger exponent = BigInteger::deserialize(stream);
	if (stream.tellg() >= stream_size) {
		result.setPrivateExponent(exponent);
		return result;
	}
	result.setPublicExponent(exponent);
	BigInteger private_exponent = BigInteger::deserialize(stream);
	result.setPrivateExponent(private_exponent);
	return result;
}

RSA RSA::deserializeKeyPair(std::istream& stream) {
	RSA result = RSA::deserializePublicKey(stream);
	BigInteger private_exp = BigInteger::deserialize(stream);
	result.setPrivateExponent(private_exp);
	return result;
}

RSA RSA::combine(const RSA& private_key, const RSA& public_key) {
	if (private_key.m_modulus != public_key.m_modulus) {
		throw std::runtime_error(
			"Cannot combine keys: their moduli are different");
	}
	RSA result = RSA(private_key.m_modulus);
	if (!private_key.m_private_exp) {
		throw std::runtime_error(
			"Cannot combine keys: private key's exponent is not specified");
	}
	if (!public_key.m_public_exp) {
		throw std::runtime_error(
			"Cannot combine keys: public key's exponent is not specified");
	}
	result.setPrivateExponent(*private_key.m_private_exp);
	result.setPublicExponent(*public_key.m_public_exp);
	return result;
}