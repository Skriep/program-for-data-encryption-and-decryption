#include "main.h"

int main(int argc, char* argv[]) {
	if (argc == 0) {
		printUsage("<program>");
		return 0;
	}
	std::string prog_name = argv[0];
	if (argc < 2) {
		printUsage(prog_name);
		return 0;
	}
	std::string cmd = argv[1];

	argc--;
	argv++;

	try {
		argparse::ArgumentParser parser(prog_name + " " + cmd);
		if (cmd == "encrypt") {
			parser.add_argument("--input", "-i")
				.help(
					"Specify input file name, or plain text to be encrypted if "
					"flag --plaintext is used")
				.required();
			parser.add_argument("--plaintext", "-p")
				.help(
					"Changes behaviour of --input argument so that its value "
					"is "
					"taken as plain text to be encrypted")
				.default_value(false)
				.implicit_value(true);
			parser.add_argument("--output", "-o")
				.help("Specify output file name")
				.required();
			parser.add_argument("--key", "-k")
				.help(
					"Specify key file, containing either public key or keypair")
				.required();

			try {
				parser.parse_args(argc, argv);
			} catch (std::runtime_error& e) {
				std::cerr << e.what() << std::endl;
				std::cout << parser;
				return 0;
			}
			std::string in_val = parser.get("-i");
			std::string out_val = parser.get("-o");
			std::string key_fname = parser.get("-k");

			std::ifstream keyfile(key_fname, std::ios::in | std::ios::binary);
			if (!keyfile.is_open()) {
				std::cerr << "Cannot open key file " << key_fname << std::endl;
				return 1;
			}
			keyfile.exceptions(std::ios::eofbit | std::ios::failbit |
			                   std::ios::badbit);
			RSA rsa = RSA::deserializePublicKey(keyfile);

			std::ofstream out_file(
				out_val, std::ios::out | std::ios::binary | std::ios::trunc);
			if (!out_file.is_open()) {
				std::cerr << "Cannot open output file " << out_val << std::endl;
				return 1;
			}
			out_file.exceptions(std::ios::failbit | std::ios::badbit);

			if (parser["--plaintext"] == true) {
				std::istringstream in_stream(in_val,
				                             std::ios::in | std::ios::binary);
				in_stream.exceptions(std::ios::eofbit | std::ios::failbit |
				                     std::ios::badbit);
				rsa.encrypt(in_stream, out_file);
			} else {
				std::ifstream in_file(in_val, std::ios::in | std::ios::binary);
				if (!in_file.is_open()) {
					std::cerr << "Cannot open input file " << in_val
							  << std::endl;
					return 1;
				}
				in_file.exceptions(std::ios::eofbit | std::ios::failbit |
				                   std::ios::badbit);
				rsa.encrypt(in_file, out_file);
			}
			std::cout << "Data has been encrypted successfully!" << std::endl;
		} else if (cmd == "decrypt") {
			parser.add_argument("--input", "-i")
				.help("Specify input file name (with encrypted data)")
				.required();
			parser.add_argument("--output", "-o")
				.help(
					"Specify output file name, if flag --plaintext is not used")
				.default_value("");
			parser.add_argument("--plaintext", "-p")
				.help(
					"Allows for --output argument to be omitted: standard "
					"output will be used instead")
				.default_value(false)
				.implicit_value(true);
			parser.add_argument("--key", "-k")
				.help(
					"Specify key file, containing either private key or "
					"keypair")
				.required();

			try {
				parser.parse_args(argc, argv);
			} catch (std::runtime_error& e) {
				std::cerr << e.what() << std::endl;
				std::cout << parser;
				return 0;
			}
			std::string in_val = parser.get("-i");
			std::string key_fname = parser.get("-k");

			std::ifstream keyfile(key_fname, std::ios::in | std::ios::binary);
			if (!keyfile.is_open()) {
				std::cerr << "Cannot open key file " << key_fname << std::endl;
				return 1;
			}
			keyfile.exceptions(std::ios::eofbit | std::ios::failbit |
			                   std::ios::badbit);
			RSA rsa = RSA::deserializePrivateOrPair(keyfile);

			std::ifstream in_file(in_val, std::ios::in | std::ios::binary);
			if (!in_file.is_open()) {
				std::cerr << "Cannot open input file " << in_val << std::endl;
				return 1;
			}
			in_file.exceptions(std::ios::eofbit | std::ios::failbit |
			                   std::ios::badbit);

			if (parser["--plaintext"] == true) {
				std::ostringstream out_stream(std::ios::out | std::ios::binary);
				out_stream.exceptions(std::ios::failbit | std::ios::badbit);
				rsa.decrypt(in_file, out_stream);
				std::cout << out_stream.str();
			} else {
				if (!parser.is_used("-o")) {
					std::cerr << "-o: required." << std::endl;
					std::cout << parser;
					return 1;
				}
				std::ofstream out_file(parser.get("-o"), std::ios::out |
				                                             std::ios::binary |
				                                             std::ios::trunc);
				if (!out_file.is_open()) {
					std::cerr << "Cannot open output file " << parser.get("-o")
							  << std::endl;
					return 1;
				}
				out_file.exceptions(std::ios::eofbit | std::ios::failbit |
				                    std::ios::badbit);
				rsa.decrypt(in_file, out_file);
				std::cout << "Data has been decrypted successfully!"
						  << std::endl;
			}
		} else if (cmd == "keygen") {
			parser.add_argument("--length", "-l")
				.help("Specify key length in bits")
				.default_value((std::size_t)128)
				.action([](const std::string& value) {
					std::size_t result = 128;
					std::istringstream iss(value);
					if (!(iss >> result)) {
						std::cerr
							<< "Incorrect value of --length. Default value 128 "
							   "will be used"
							<< std::endl;
					}
					return result;
				});
			parser.add_argument("--output", "-o")
				.help(
					"Specify output file name in which keypair will be stored")
				.required();
			parser.add_argument("--private", "-pr")
				.help("Specify output file name for private key")
				.default_value("");
			parser.add_argument("--public", "-pub")
				.help("Specify output file name for public key")
				.default_value("");

			try {
				parser.parse_args(argc, argv);
			} catch (std::runtime_error& e) {
				std::cerr << e.what() << std::endl;
				std::cout << parser;
				return 0;
			}

			std::string out_fname = parser.get("-o");
			std::ofstream out_file(
				out_fname, std::ios::out | std::ios::binary | std::ios::trunc);
			if (!out_file.is_open()) {
				std::cerr << "Cannot open output file " << out_fname
						  << std::endl;
				return 1;
			}
			out_file.exceptions(std::ios::failbit | std::ios::badbit);

			RSA rsa = RSA::generateKeyPair(parser.get<std::size_t>("-l"));
			rsa.serializeKeyPair(out_file);

			if (parser.is_used("-pr")) {
				std::ofstream prkey_file(
					parser.get("-pr"),
					std::ios::out | std::ios::binary | std::ios::trunc);
				if (!prkey_file.is_open()) {
					std::cerr << "Cannot open output file " << out_fname
							  << std::endl;
					return 1;
				}
				prkey_file.exceptions(std::ios::failbit | std::ios::badbit);
				rsa.serializePrivateKey(prkey_file);
			}
			if (parser.is_used("-pub")) {
				std::ofstream pubkey_file(
					parser.get("-pub"),
					std::ios::out | std::ios::binary | std::ios::trunc);
				if (!pubkey_file.is_open()) {
					std::cerr << "Cannot open output file " << out_fname
							  << std::endl;
					return 1;
				}
				pubkey_file.exceptions(std::ios::failbit | std::ios::badbit);
				rsa.serializePublicKey(pubkey_file);
			}
			std::cout << "Operation completed successfully!" << std::endl;
		} else if (cmd == "keymgr") {
			parser.add_argument("--split", "-s")
				.help(
					"Splits keypair file into files with private and public "
					"keys")
				.default_value(false)
				.implicit_value(true);
			parser.add_argument("--combine", "-c")
				.help(
					"Combines files with private and public keys into keypair "
					"file")
				.default_value(false)
				.implicit_value(true);
			parser.add_argument("--keypair", "-kp")
				.help("Specify file name of keypair")
				.required();
			parser.add_argument("--private", "-pr")
				.help("Specify file name of private key")
				.required();
			parser.add_argument("--public", "-pub")
				.help("Specify file name of public key")
				.required();

			try {
				parser.parse_args(argc, argv);
			} catch (std::runtime_error& e) {
				std::cerr << e.what() << std::endl;
				std::cout << parser;
				return 0;
			}

			if (parser.is_used("--combine") && parser.is_used("--split")) {
				std::cerr << "Please use only one flag: --combine or --split"
						  << std::endl;
				std::cout << parser;
				return 1;
			}

			if (parser.is_used("--split")) {
				std::ifstream keypair_file(parser.get("-kp"),
				                           std::ios::in | std::ios::binary);
				if (!keypair_file.is_open()) {
					std::cerr << "Cannot open input file " << parser.get("-kp")
							  << std::endl;
					return 1;
				}
				keypair_file.exceptions(std::ios::eofbit | std::ios::failbit |
				                        std::ios::badbit);
				std::ofstream prkey_file(
					parser.get("-pr"),
					std::ios::out | std::ios::binary | std::ios::trunc);
				if (!prkey_file.is_open()) {
					std::cerr << "Cannot open output file " << parser.get("-pr")
							  << std::endl;
					return 1;
				}
				prkey_file.exceptions(std::ios::failbit | std::ios::badbit);
				std::ofstream pubkey_file(
					parser.get("-pub"),
					std::ios::out | std::ios::binary | std::ios::trunc);
				if (!pubkey_file.is_open()) {
					std::cerr << "Cannot open output file "
							  << parser.get("-pub") << std::endl;
					return 1;
				}
				pubkey_file.exceptions(std::ios::failbit | std::ios::badbit);

				RSA rsa = RSA::deserializeKeyPair(keypair_file);
				rsa.serializePrivateKey(prkey_file);
				rsa.serializePublicKey(pubkey_file);
			} else if (parser.is_used("--combine")) {
				std::ofstream keypair_file(
					parser.get("-kp"),
					std::ios::out | std::ios::binary | std::ios::trunc);
				if (!keypair_file.is_open()) {
					std::cerr << "Cannot open input file " << parser.get("-kp")
							  << std::endl;
					return 1;
				}
				keypair_file.exceptions(std::ios::failbit | std::ios::badbit);
				std::ifstream prkey_file(parser.get("-pr"),
				                         std::ios::in | std::ios::binary);
				if (!prkey_file.is_open()) {
					std::cerr << "Cannot open output file " << parser.get("-pr")
							  << std::endl;
					return 1;
				}
				prkey_file.exceptions(std::ios::eofbit | std::ios::failbit |
				                      std::ios::badbit);
				std::ifstream pubkey_file(parser.get("-pub"),
				                          std::ios::in | std::ios::binary);
				if (!pubkey_file.is_open()) {
					std::cerr << "Cannot open output file "
							  << parser.get("-pub") << std::endl;
					return 1;
				}
				pubkey_file.exceptions(std::ios::eofbit | std::ios::failbit |
				                       std::ios::badbit);

				RSA private_rsa = RSA::deserializePrivateKey(prkey_file);
				RSA public_rsa = RSA::deserializePublicKey(pubkey_file);
				RSA combined_rsa = RSA::combine(private_rsa, public_rsa);
				combined_rsa.serializeKeyPair(keypair_file);
			} else {
				std::cerr << "Please use --combine or --split flag"
						  << std::endl;
				std::cout << parser;
				return 1;
			}
			std::cout << "Operation completed successfully!" << std::endl;
		} else {
			std::cerr << "Unknown command: " << cmd << std::endl;
			printUsage(prog_name);
			return 1;
		}
	} catch (std::runtime_error& e) {
		std::cerr << "Runtime error:\n" << e.what() << std::endl;
		return 1;
	}

	return 0;
}

void printUsage(const std::string& prog_name) {
	std::cout << "Usage: " << prog_name << " <command> [options]\n"
			  << std::endl;
	printCommands(prog_name);
}

void printCommands(const std::string& prog_name) {
	const std::string text =
		"Available commands:\n\
  encrypt   Encrypt file or message\n\
  decrypt   Decrypt file\n\
  keygen    Generate RSA keypair\n\
  keymgr    Split keypair file into private and public keys, or combine two keys into one file\n\
	\n\
Use '" + prog_name +
		" <command> --help' to get list of options for any command.";
	std::cout << text << std::endl;
}