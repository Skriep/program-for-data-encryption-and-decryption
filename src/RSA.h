#if !defined(RSAH)
#define RSAH

#include <sstream>

#include "CryptoMath.h"

class RSA {
   private:
	BigInteger m_modulus;
	std::unique_ptr<BigInteger> m_public_exp;
	std::unique_ptr<BigInteger> m_private_exp;

   public:
	RSA(const BigInteger& modulus);
	RSA(const BigInteger& modulus, const BigInteger& public_exp,
	    const BigInteger& private_exp);
	RSA(const RSA& rsa);
	void setPublicExponent(const BigInteger& public_exp);
	void setPrivateExponent(const BigInteger& private_exp);
	BigInteger encrypt(const BigInteger& msg);
	void encrypt(std::istream& data_in, std::ostream& encrypted_out);
	BigInteger decrypt(const BigInteger& enc);
	void decrypt(std::istream& encrypted_in, std::ostream& data_out);
	RSA operator=(const RSA& rsa);

	static RSA generateKeyPair(std::size_t mod_bits_count);

	void serializePublicKey(std::ostream& stream);
	void serializePrivateKey(std::ostream& stream);
	void serializeKeyPair(std::ostream& stream);

	static RSA deserializePublicKey(std::istream& stream);
	static RSA deserializePrivateKey(std::istream& stream);
	static RSA deserializePrivateOrPair(std::istream& stream);
	static RSA deserializeKeyPair(std::istream& stream);

	static RSA combine(const RSA& private_key, const RSA& public_key);
};

#endif  // RSAH
