#include "CryptoMath.h"

BigInteger CryptoMath::binpow(const BigInteger& base, const BigInteger& exp,
                              const BigInteger& mod) {
	BigInteger res = 1;
	BigInteger curr_base = base % mod;
	for (std::size_t i = 0; i < exp.m_size; i++) {
		for (std::size_t j = 0; j < 8; j++) {
			if (exp.m_bytes[i][j]) {
				res = (res * curr_base) % mod;
			}
			curr_base = (curr_base * curr_base) % mod;
		}
	}
	return res;
}

BigInteger CryptoMath::inverse(BigInteger x, const BigInteger& m) {
	BigInteger a = 0, b = m, u = 1;
	while (x > 0) {
		BigInteger tmp;
		BigInteger q = BigInteger::div(b, x, tmp);
		b = x;
		x = tmp;
		BigInteger a_cpy = a;
		a = u;
		u = a_cpy - q * u;
	}
	if (b == 1) {
		a = a % m;
		if (a < 0) {
			a += m;
		}
		return a % m;
	}
	throw std::runtime_error(
		"Cannot get inverse of x mod m: x and m are not coprime!");
}

bool CryptoMath::isPrime(const BigInteger& number, int rounds,
                         bool print_state) {
	if (number < 0) {
		return isPrime(-number, rounds, print_state);
	}
	if (number <= 1) {
		return false;
	}
	if (number <= 3) {
		return true;
	}

	static const BigInteger primes[] = {2,  3,  5,  7,  11, 13,
	                                    17, 19, 23, 29, 31, 37};
	static const int primes_count = 12;
	for (int i = 0; i < primes_count; i++) {
		if (number % primes[i] == 0) {
			return (number == primes[i]);
		}
	}

	if (number.m_size == 1) {
		return true;
	}

	const BigInteger number_minus_one = number - 1;

	// number equals (pow(2, r) * d + 1)
	std::size_t r = 0;
	BigInteger d = number_minus_one;
	while (!d.m_bytes[0][0]) {
		d >>= 1;
		r++;
	}

	std::random_device rd;
	std::mt19937 rand_engine(rd());

	std::size_t MSbyte_num = number.m_size - 1;
	while (MSbyte_num > 0 && number.m_bytes[MSbyte_num] == 0) {
		MSbyte_num--;
	}
	std::size_t MSbit_num = 7;
	while (MSbit_num > 0 && !number.m_bytes[MSbyte_num][MSbit_num]) {
		MSbit_num--;
	}

	for (int i = 1; i <= rounds; i++) {
		if (print_state && i % 5 == 0) {
			std::cout << "+" << std::flush;
		}
		BigInteger rand_num =
			getRandomNumber(rand_engine, MSbyte_num * 8 + MSbit_num);
		if (rand_num < 2) {
			continue;
		}
		BigInteger x = CryptoMath::binpow(rand_num, d, number);
		if (x == 1 || x == number_minus_one) {
			continue;
		}
		bool flag = false;
		for (std::size_t j = 0; j < r - 1; j++) {
			x = (x * x) % number;
			if (x == number_minus_one) {
				flag = true;
				break;
			}
		}
		if (!flag) {
			return false;
		}
	}
	return true;
}

BigInteger CryptoMath::getRandomPrime(std::size_t bits_count,
                                      bool print_state) {
	std::random_device rd;
	while (true) {
		BigInteger rand_num = getRandomNumber(rd, bits_count);
		rand_num.m_bytes[0] |= 1;
		if (isPrime(rand_num, 45, print_state)) {
			if (print_state) {
				std::cout << std::endl;
			}
			return rand_num;
		}
		if (print_state) {
			std::cout << "." << std::flush;
		}
	}
}
