#include "BigInteger.h"

BigInteger::BigInteger(const BigInteger& n) {
	Byte* new_bytes = new Byte[n.getSize()];
	for (std::size_t i = 0; i < n.getSize(); i++) {
		new_bytes[i] = n.m_bytes[i];
	}
	m_bytes.reset(new_bytes);
	m_size = n.getSize();
}

std::size_t BigInteger::getSize() const { return m_size; }

bool BigInteger::resize(std::size_t new_size) {
	if (new_size == 0) {
		return false;
	}
	if (m_size == new_size) {
		return true;
	}
	std::unique_ptr<Byte[]> new_bytes = std::make_unique<Byte[]>(new_size);

	if (new_size > m_size) {
		for (std::size_t i = 0; i < m_size; i++) {
			new_bytes[i] = m_bytes[i];
		}
		if (new_bytes[m_size - 1][7]) {
			for (std::size_t i = m_size; i < new_size; i++) {
				new_bytes[i] = 0b11111111;
			}
		}
	} else {
		for (std::size_t i = 0; i < new_size; i++) {
			new_bytes[i] = m_bytes[i];
		}
		if (new_bytes[new_size - 1][7] != m_bytes[m_size - 1][7]) {
			return false;
		}

		bool extra_byte =
			(new_bytes[new_size - 1][7] ? 0b11111111 : 0b00000000);
		for (std::size_t i = new_size; i < m_size; i++) {
			if (m_bytes[i] != extra_byte) {
				return false;
			}
		}
	}

	m_bytes.swap(new_bytes);
	m_size = new_size;
	return true;
}

bool BigInteger::reduce() {
	std::size_t new_size = m_size;
	bool sign_bit = m_bytes[m_size - 1][7];
	bool extra_byte = (sign_bit ? 0b11111111 : 0b00000000);
	while (new_size >= 2 && m_bytes[new_size - 1] == extra_byte &&
	       m_bytes[new_size - 2][7] == sign_bit) {
		new_size--;
	}
	return resize(new_size);
}

bool BigInteger::operator<(const BigInteger& b) const {
	return (BigInteger::compare(*this, b) == -1);
}

bool BigInteger::operator>(const BigInteger& b) const { return (b < *this); }

bool BigInteger::operator<=(const BigInteger& b) const { return !(b < *this); }

bool BigInteger::operator>=(const BigInteger& b) const { return !(*this < b); }

bool BigInteger::operator==(const BigInteger& b) const {
	return (BigInteger::compare(*this, b) == 0);
}

bool BigInteger::operator!=(const BigInteger& b) const { return !(*this == b); }

BigInteger& BigInteger::operator++() {
	bool prev_carry = 0;
	bool carry = 0;
	m_bytes[0] = Byte::sum(m_bytes[0], 1, carry, prev_carry);
	for (std::size_t i = 1; i < m_size; i++) {
		if (!carry && !prev_carry) {
			break;
		}
		m_bytes[i] = Byte::sum(m_bytes[i], 0, carry, prev_carry);
	}

	// Overflow
	if (prev_carry != carry) {
		if (!resize(m_size + 1)) {
			throw std::runtime_error("Cannot resize BigInteger");
		}
		m_bytes[m_size - 1] = (carry ? 0b11111111 : 0b00000000);
	}
	return *this;
}

BigInteger BigInteger::operator+(const BigInteger& b) const {
	std::size_t this_size = m_size;
	std::size_t b_size = b.m_size;
	std::size_t min_size = (this_size < b_size ? this_size : b_size);
	std::size_t max_size = (this_size < b_size ? b_size : this_size);

	BigInteger res;
	if (!res.resize(max_size)) {
		throw std::runtime_error("Cannot resize BigInteger");
	}

	bool prev_carry = 0;
	bool carry = 0;
	for (std::size_t i = 0; i < min_size; i++) {
		res.m_bytes[i] = Byte::sum(m_bytes[i], b.m_bytes[i], carry, prev_carry);
	}

	Byte b_byte = (b.m_bytes[b.m_size - 1][7] ? 0b11111111 : 0b00000000);
	for (std::size_t i = min_size; i < this_size; i++) {
		res.m_bytes[i] = Byte::sum(m_bytes[i], b_byte, carry, prev_carry);
	}

	Byte this_byte = (m_bytes[m_size - 1][7] ? 0b11111111 : 0b00000000);
	for (std::size_t i = min_size; i < b_size; i++) {
		res.m_bytes[i] = Byte::sum(this_byte, b.m_bytes[i], carry, prev_carry);
	}

	// Overflow
	if (prev_carry != carry) {
		if (!res.resize(max_size + 1)) {
			throw std::runtime_error("Cannot resize BigInteger");
		}
		res.m_bytes[res.m_size - 1] = (carry ? 0b11111111 : 0b00000000);
	}
	return res;
}

BigInteger& BigInteger::operator+=(const BigInteger& b) {
	*this = *this + b;
	return *this;
}

BigInteger BigInteger::operator-() const {
	BigInteger negated;
	if (!negated.resize(m_size)) {
		throw std::runtime_error("Cannot resize BigInteger");
	}
	for (std::size_t i = 0; i < m_size; i++) {
		negated.m_bytes[i] = ~m_bytes[i];
	}
	return ++negated;
}

BigInteger BigInteger::operator-(const BigInteger& b) const {
	return *this + (-b);
}

BigInteger& BigInteger::operator-=(const BigInteger& b) {
	*this = *this - b;
	return *this;
}

BigInteger BigInteger::operator*(const BigInteger& b) const {
	if (b.m_bytes[b.m_size - 1][7]) {
		return (-*this) * (-b);
	}

	std::size_t this_size = m_size;
	std::size_t b_size = b.m_size;

	BigInteger res;
	if (!res.resize(this_size + b_size)) {
		throw std::runtime_error("Cannot resize BigInteger");
	}

	bool this_sign_bit = m_bytes[m_size - 1][7];

	for (std::size_t i = 0; i < b_size; i++) {
		for (std::size_t j = 0; j < 8; j++) {
			if (!b.m_bytes[i][j]) {
				continue;
			}
			bool carry = 0;
			std::size_t res_byte_num = i;
			std::size_t res_bit_num = j;
			for (std::size_t k = 0; k < this_size; k++) {
				for (std::size_t t = 0; t < 8; t++) {
					res_byte_num = i + k;
					res_bit_num = j + t;
					if (res_bit_num > 7) {
						res_bit_num -= 8;
						res_byte_num++;
					}
					bool new_carry = (res.m_bytes[res_byte_num][res_bit_num] &&
					                  m_bytes[k][t]);
					bool x =
						res.m_bytes[res_byte_num][res_bit_num] ^ m_bytes[k][t];
					res.m_bytes[res_byte_num] ^=
						((m_bytes[k][t] ^ carry) << res_bit_num);
					carry = new_carry || (x && carry);
				}
			}
			res_bit_num++;
			if (res_bit_num > 7) {
				res_bit_num -= 8;
				res_byte_num++;
			}
			while ((carry || this_sign_bit) && res_byte_num < res.m_size) {
				bool new_carry =
					(res.m_bytes[res_byte_num][res_bit_num] && this_sign_bit);
				bool x = res.m_bytes[res_byte_num][res_bit_num] ^ this_sign_bit;
				res.m_bytes[res_byte_num] ^=
					((this_sign_bit ^ carry) << res_bit_num);
				carry = new_carry || (x && carry);
				res_bit_num++;
				if (res_bit_num > 7) {
					res_bit_num -= 8;
					res_byte_num++;
				}
			}
		}
	}
	return res;
}

BigInteger& BigInteger::operator*=(const BigInteger& b) {
	*this = (*this) * b;
	return *this;
}

BigInteger BigInteger::operator/(const BigInteger& b) const {
	BigInteger rem;
	return BigInteger::div(*this, b, rem);
}

BigInteger& BigInteger::operator/=(const BigInteger& b) {
	*this = (*this) / b;
	return *this;
}

BigInteger BigInteger::operator%(const BigInteger& b) const {
	BigInteger rem;
	BigInteger::div(*this, b, rem);
	return rem;
}

BigInteger& BigInteger::operator%=(const BigInteger& b) {
	*this = (*this) % b;
	return *this;
}

BigInteger& BigInteger::operator<<=(std::size_t n) {
	for (std::size_t i = 0; i < n; i++) {
		bool bit = 0;
		std::size_t byte_num = 0;
		while (byte_num < m_size - 1) {
			bool next_bit = m_bytes[byte_num][7];
			m_bytes[byte_num] <<= 1;
			m_bytes[byte_num] |= bit;
			bit = next_bit;
			byte_num++;
		}
		if (m_bytes[byte_num][7] != m_bytes[byte_num][6]) {
			if (!resize(m_size + 1)) {
				throw std::runtime_error("Cannot resize BigInteger");
			}
		}
		m_bytes[byte_num] <<= 1;
		m_bytes[byte_num] |= bit;
	}
	return *this;
}

BigInteger& BigInteger::operator>>=(std::size_t n) {
	for (std::size_t i = 0; i < n; i++) {
		bool bit = 0;
		std::size_t byte_num = m_size;
		while (byte_num-- > 0) {
			bool next_bit = m_bytes[byte_num][0];
			m_bytes[byte_num] >>= 1;
			m_bytes[byte_num] |= (bit << 7);
			bit = next_bit;
		}
		if (m_size > 1 && m_bytes[m_size - 1] == 0 && !m_bytes[m_size - 2][7]) {
			if (!resize(m_size - 1)) {
				throw std::runtime_error("Cannot resize BigInteger");
			}
		}
	}
	return *this;
}

BigInteger BigInteger::operator=(const BigInteger& n) {
	Byte* new_bytes = new Byte[n.getSize()];
	for (std::size_t i = 0; i < n.getSize(); i++) {
		new_bytes[i] = n.m_bytes[i];
	}
	m_bytes.reset(new_bytes);
	m_size = n.getSize();
	return *this;
}

void BigInteger::serialize(std::ostream& stream) {
	uint64_t size = m_size;
	stream.write((char*)&size, sizeof(size));
	for (std::size_t i = 0; i < m_size; i++) {
		m_bytes[i].serialize(stream);
	}
}

BigInteger BigInteger::deserialize(std::istream& stream) {
	uint64_t size = 0;
	stream.read((char*)&size, sizeof(size));
	BigInteger result;
	if (!result.resize((std::size_t)size)) {
		throw std::runtime_error("Cannot resize BigInteger");
	}
	for (std::size_t i = 0; i < result.m_size; i++) {
		result.m_bytes[i] = Byte::deserialize(stream);
	}
	return result;
}

int BigInteger::compare(const BigInteger& a, const BigInteger& b) {
	bool a_sign_bit = a.m_bytes[a.m_size - 1][7];
	bool b_sign_bit = b.m_bytes[b.m_size - 1][7];

	if (a_sign_bit != b_sign_bit) {
		return (a_sign_bit ? -1 : 1);
	}

	std::size_t min_size = a.m_size;

	if (a.m_size < b.m_size) {
		Byte a_byte = (a_sign_bit ? 0b11111111 : 0b00000000);
		std::size_t i = b.m_size;
		while (i-- > min_size) {
			int compared = Byte::compare(a_byte, b.m_bytes[i]);
			if (compared != 0) {
				return compared;
			}
		}
	} else if (a.m_size > b.m_size) {
		min_size = b.m_size;
		Byte b_byte = (b_sign_bit ? 0b11111111 : 0b00000000);
		std::size_t i = a.m_size;
		while (i-- > min_size) {
			int compared = Byte::compare(a.m_bytes[i], b_byte);
			if (compared != 0) {
				return compared;
			}
		}
	}
	std::size_t i = min_size;
	while (i-- > 0) {
		int compared = Byte::compare(a.m_bytes[i], b.m_bytes[i]);
		if (compared != 0) {
			return compared;
		}
	}
	return 0;
}

void BigInteger::printBytes() const {
	std::size_t i = m_size;
	while (i-- > 0) {
		std::size_t j = 8;
		while (j-- > 0) {
			std::cout << m_bytes[i][j];
		}
		std::cout << " ";
	}
	std::cout << std::endl;
}

BigInteger BigInteger::div(const BigInteger& a, const BigInteger& b,
                           BigInteger& rem) {
	if (b == 0) {
		throw std::runtime_error("Cannot divide by 0");
	}

	bool a_sign_bit = a.m_bytes[a.m_size - 1][7];
	bool b_sign_bit = b.m_bytes[b.m_size - 1][7];

	if (a_sign_bit != b_sign_bit) {
		if (a_sign_bit) {
			BigInteger result = -div(-a, b, rem);
			rem = -rem;
			return result;
		} else {
			return -div(a, -b, rem);
		}
	} else if (a_sign_bit && b_sign_bit) {
		BigInteger result = -div(-a, b, rem);
		rem = -rem;
		return result;
	}

	BigInteger res;
	BigInteger divident;

	std::size_t a_byte = a.m_size - 1;
	std::size_t a_bit = 7;

	bool is_last_bit = false;

	while (divident < b) {
		divident <<= 1;
		if (a.m_bytes[a_byte][a_bit]) {
			++divident;
		}
		if (a_bit == 0) {
			if (a_byte == 0) {
				is_last_bit = true;
				break;
			}
			a_bit = 7;
			a_byte--;
		} else {
			a_bit--;
		}
	}

	while (true) {
		res <<= 1;
		if (divident >= b) {
			divident -= b;
			divident.reduce();
			++res;
		}

		if (is_last_bit) {
			break;
		}

		divident <<= 1;
		if (a.m_bytes[a_byte][a_bit]) {
			++divident;
		}

		if (a_bit == 0) {
			if (a_byte == 0) {
				is_last_bit = true;
				continue;
			}
			a_bit = 7;
			a_byte--;
		} else {
			a_bit--;
		}
	}
	rem = divident;
	return res;
}
