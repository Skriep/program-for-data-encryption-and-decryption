OUT_FILENAME:=program
SDIR:=src
SRCS:=main.cpp Byte.cpp BigInteger.cpp CryptoMath.cpp RSA.cpp
SRCS:=$(patsubst %, $(SDIR)/%, $(SRCS))
FLAGS:=-std=c++17 -pedantic-errors -Iinclude

compile: argparse.o
	g++ $(FLAGS) $(SRCS) -o $(OUT_FILENAME)

argparse.o: include/argparse/argparse.hpp
	g++ -std=c++17 $< -o argparse.o

clean:
	rm -rf $(OUT_FILENAME)
